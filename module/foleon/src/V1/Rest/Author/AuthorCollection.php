<?php
namespace foleon\V1\Rest\Author;

use DoctrineModule\Controller\DoctrineClass;
use Zend\Paginator\Paginator;

class AuthorCollection extends Paginator
{
  public static function load($id) {

    // It would be nicer to fetch the Doctrine class from a dependency injection container.
    $doctrine = new DoctrineClass();

    // Return loaded entity.
    return $doctrine->entityManager->getRepository('foleon\V1\Rest\Author\AuthorEntity')
      ->find($id);
  }

  public static function save($data) {

    // Maybe it would be nicer to fetch the Doctrine class from a dependency injection container.
    $doctrine = new DoctrineClass();

    // Create the book entity.
    $author = new AuthorEntity();

    $author->setName($data->name);
    $doctrine->entityManager->persist($author);
    $doctrine->entityManager->flush();
  }
}
