<?php

namespace foleon\V1\Rest\Author;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="author")
 */

class AuthorEntity
{

  /**
   * @Column(type="integer")
   * @Id
   * @GeneratedValue(strategy="AUTO")
   */
  public $id;

  /**
   * @Column(type="string", length=100)
   */
  public $name;

  /**
   *
   * @OneToMany(targetEntity="foleon\V1\Rest\Book\BookEntity", mappedBy="author", fetch="LAZY")
   */
  public $books;

  public function __construct() {
    $this->books = new ArrayCollection();
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $books
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getBooks() {
    $booksTemp = [];
    foreach ($this->books as $book) {

      $booksTemp[] = ['title' => $book->title, 'id' => $book->id];
    }
    return $booksTemp;
  }

  /**
   * @param mixed $books
   */
  public function setBooks($books) {
    $this->books = $books;
  }



}
