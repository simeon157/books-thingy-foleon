<?php

namespace foleon\V1\Rest\Book;

/**
 * @Entity
 * @Table(name="book")
 */

class BookEntity
{

  /**
   * @Column(type="integer")
   * @Id
   * @GeneratedValue(strategy="AUTO")
   */
  public $id;

  /**
   * @Column(type="string", length=100)
   */
  public $title;

  /**
   * @ManyToOne(targetEntity="foleon\V1\Rest\Author\AuthorEntity", inversedBy="books", fetch="EAGER")
   * @JoinColumn(name="author", referencedColumnName="id")
   */
  public $author;


  /**
   * @return mixed
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param mixed $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return mixed
   */
  public function getAuthor() {
    return $this->author;
  }

  /**
   * @param mixed $author
   *
   * @throws \Exception
   */
  public function setAuthor($author) {

    if($author === NULL) {
      throw new \Exception('Author is not in database.');
    }
    $this->author = $author;

  }
}
