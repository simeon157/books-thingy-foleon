<?php

namespace foleon\V1\Rest\Book;

use DoctrineModule\Controller\DoctrineClass;
use Zend\Paginator\Paginator;

/**
 * Class BookCollection
 *
 * @package foleon\V1\Rest\Book
 *
 * Using this class as
 */
class BookCollection extends Paginator {

  public static function load($id) {

    // It would be nicer to fetch the Doctrine class from a dependency injection container.
    $doctrine = new DoctrineClass();

    // Return loaded entity.
    return $doctrine->entityManager->getRepository('foleon\V1\Rest\Book\BookEntity')
      ->find($id);
  }

  public static function save($data) {

    // Maybe it would be nicer to fetch the Doctrine class from a dependency injection container.
    $doctrine = new DoctrineClass();

    // Create the book entity.
    $book = new BookEntity();

    // Load author object from DB.
    $book->setTitle($data->title);
    $author = $doctrine->entityManager->getRepository('foleon\V1\Rest\Author\AuthorEntity')
      ->find($data->author);
    if ($author === NULL) {
      throw new \Exception('Author doesn\'t exist.');
    }
    $book->setAuthor($author);

    // Persist the created entity.
    $doctrine->entityManager->persist($book);
    $doctrine->entityManager->flush();
  }
}
