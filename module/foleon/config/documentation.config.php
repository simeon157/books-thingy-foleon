<?php
return [
    'foleon\\V1\\Rest\\Book\\Controller' => [
        'description' => 'Create and fetch books.',
        'collection' => [
            'GET' => [
                'description' => 'Gets a book and it\'s author.',
                'response' => '{
    "title": [book name],
    "author": {
        "id": [author id],
        "name": [author name]
    },
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/book/[book_id]"
        }
    }
}',
            ],
            'POST' => [
                'description' => 'Create a book and assign an author.',
                'request' => '{
	"title":[book title],
	"author": [author id]
}',
                'response' => '{
    "title": [book title],
    "author": [author id],
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/book"
        }
    }
}',
            ],
            'description' => '',
        ],
        'entity' => [
            'description' => 'Endpoint for creating and fetching a book',
            'GET' => [
                'description' => 'Gets a book and it\'s author.',
                'response' => '{
    "title": [book name],
    "author": {
        "id": [author id],
        "name": [author name]
    },
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/book/[book_id]"
        }
    }
}',
            ],
            'POST' => [
                'description' => 'Create a book and assign an author.',
                'request' => '{
	"title":[book title],
	"author": [author id]
}',
                'response' => '{
    "title": [book title],
    "author": [author id],
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/book"
        }
    }
}',
            ],
        ],
    ],
    'foleon\\V1\\Rest\\Author\\Controller' => [
        'collection' => [
            'GET' => [
                'description' => 'Gets an author',
                'response' => '{
    "name": [author name]
    "id": [author id]
    "books": [
        {
            "title": [book title]
            "id": [book id]
        },
    ],
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/author/[author id]"
        }
    }
}',
            ],
            'POST' => [
                'description' => 'Creates an author',
                'request' => '{
	"name":[author name]
}',
                'response' => '{
    "name": [author name]
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/author"
        }
    }
}',
            ],
            'description' => 'Endpoint for creating and fetching an author',
        ],
        'entity' => [
            'description' => 'Endpoint for creating and fetching an author',
            'GET' => [
                'description' => 'Gets an author',
                'response' => '{
    "name": [author name]
    "id": [author id]
    "books": [
        {
            "title": [book title]
            "id": [book id]
        },
    ],
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/author/[author id]"
        }
    }
}',
            ],
            'POST' => [
                'description' => 'Creates an author',
                'request' => '{
	"name":[author name]
}',
                'response' => '{
    "name": [author name]
    "_links": {
        "self": {
            "href": "http://0.0.0.0:8888/author"
        }
    }
}',
            ],
        ],
    ],
];
