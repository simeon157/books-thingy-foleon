<?php
return [
    'service_manager' => [
        'factories' => [
            \foleon\V1\Rest\Book\BookResource::class => \foleon\V1\Rest\Book\BookResourceFactory::class,
            \foleon\V1\Rest\Author\AuthorResource::class => \foleon\V1\Rest\Author\AuthorResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'foleon.rest.book' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/book[/:book_id]',
                    'defaults' => [
                        'controller' => 'foleon\\V1\\Rest\\Book\\Controller',
                    ],
                ],
            ],
            'foleon.rest.author' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/author[/:author_id]',
                    'defaults' => [
                        'controller' => 'foleon\\V1\\Rest\\Author\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'foleon.rest.book',
            1 => 'foleon.rest.author',
        ],
    ],
    'zf-rest' => [
        'foleon\\V1\\Rest\\Book\\Controller' => [
            'listener' => \foleon\V1\Rest\Book\BookResource::class,
            'route_name' => 'foleon.rest.book',
            'route_identifier_name' => 'book_id',
            'collection_name' => 'book',
            'entity_http_methods' => [
                0 => 'GET',
            ],
            'collection_http_methods' => [
                0 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \foleon\V1\Rest\Book\BookEntity::class,
            'collection_class' => \foleon\V1\Rest\Book\BookCollection::class,
            'service_name' => 'book',
        ],
        'foleon\\V1\\Rest\\Author\\Controller' => [
            'listener' => \foleon\V1\Rest\Author\AuthorResource::class,
            'route_name' => 'foleon.rest.author',
            'route_identifier_name' => 'author_id',
            'collection_name' => 'author',
            'entity_http_methods' => [
                0 => 'GET',
            ],
            'collection_http_methods' => [
                0 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \foleon\V1\Rest\Author\AuthorEntity::class,
            'collection_class' => \foleon\V1\Rest\Author\AuthorCollection::class,
            'service_name' => 'author',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'foleon\\V1\\Rest\\Book\\Controller' => 'HalJson',
            'foleon\\V1\\Rest\\Author\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'foleon\\V1\\Rest\\Book\\Controller' => [
                0 => 'application/vnd.foleon.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'foleon\\V1\\Rest\\Author\\Controller' => [
                0 => 'application/vnd.foleon.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'foleon\\V1\\Rest\\Book\\Controller' => [
                0 => 'application/vnd.foleon.v1+json',
                1 => 'application/json',
            ],
            'foleon\\V1\\Rest\\Author\\Controller' => [
                0 => 'application/vnd.foleon.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \foleon\V1\Rest\Book\BookEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'foleon.rest.book',
                'route_identifier_name' => 'book_id',
                'hydrator' => \Zend\Hydrator\ClassMethodsHydrator::class,
            ],
            \foleon\V1\Rest\Book\BookCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'foleon.rest.book',
                'route_identifier_name' => 'book_id',
                'is_collection' => true,
            ],
            \foleon\V1\Rest\Author\AuthorEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'foleon.rest.author',
                'route_identifier_name' => 'author_id',
                'hydrator' => \Zend\Hydrator\ObjectPropertyHydrator::class,
            ],
            \foleon\V1\Rest\Author\AuthorCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'foleon.rest.author',
                'route_identifier_name' => 'author_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'foleon\\V1\\Rest\\Book\\Controller' => [
            'input_filter' => 'foleon\\V1\\Rest\\Book\\Validator',
        ],
        'foleon\\V1\\Rest\\Author\\Controller' => [
            'input_filter' => 'foleon\\V1\\Rest\\Author\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'foleon\\V1\\Rest\\Book\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'title',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'author',
            ],
        ],
        'foleon\\V1\\Rest\\Author\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'name',
                'description' => 'name of author',
            ],
        ],
    ],
];
