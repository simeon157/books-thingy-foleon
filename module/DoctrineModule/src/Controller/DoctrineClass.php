<?php
namespace DoctrineModule\Controller;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class DoctrineClass
 *
 * @package DoctrineModule\Controller
 * 
 * Class providing Doctrine entity manager.
 */
class DoctrineClass extends AbstractPlugin
{
  public $entityManager;
  public function __construct() {

    $paths = array("module/foleon/src/V1/Rest/Book");
    $isDevMode = false;

    // the connection configuration
    $dbParams = array(
      'driver'   => 'pdo_mysql',
      'user'     => 'root',
      'password' => 'root',
      'dbname'   => 'foleon',
    );

    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

    $this->entityManager = EntityManager::create($dbParams, $config);
    return $this;
  }
}
