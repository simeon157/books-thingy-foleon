<?php
namespace DoctrineModule;
use DoctrineModule\Controller\DoctrineClass;
use Zend\ServiceManager\Factory\InvokableFactory;
return [
  'controllers' => [
    'factories' => [
      DoctrineClass::class => InvokableFactory::class
    ],
  ],
];