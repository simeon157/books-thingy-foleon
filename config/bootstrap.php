<?php
// bootstrap.php
require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array("module/foleon/src/V1/Rest/");
$isDevMode = false;

// the connection configuration
$dbParams = array(
  'driver'   => 'pdo_mysql',
  'user'     => 'root',
  'password' => 'root',
  'dbname'   => 'foleon',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

$entityManager = EntityManager::create($dbParams, $config);