### Setup

`composer install`

1. create DB for the Doctrine tables.
    - You can find the DB config in
    `config/boostrap.php` and `DoctrineClass.php` in Doctrine module. I use the first one to generate tables in the command line  and the second in the AuthorCollection/BookCollection classes. 
2. ```./vendor/bin/doctrine orm:schema-tool:create``` to generate schema from the Annotations in the foleon module.

3. `php -S 0.0.0.0:8888 -t public public/index.php` To run apigility on 0.0.0.0:8888

Assignment is built and tested under Ubuntu 16.04 with PHP 7.2.4 and MySQL 5.7.23

PS: Requires composer version ^1.1
